package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by matteofavaro on 05/04/2017.
 */
@SpringBootApplication(scanBasePackages={"hello"})

public class Application {
  public static void main(String[] args) throws Exception {
    SpringApplication.run(IndexController.class, args);
  }

}
