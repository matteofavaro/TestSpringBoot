package hello;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by matteofavaro on 07/04/2017.
 */
@Controller
@EnableAutoConfiguration
public class IndexController {


  @GetMapping("/")
  public String handler(Model model) {
    model.addAttribute("msg",
        "a jar packaging example");
    return "index";
  }


}
