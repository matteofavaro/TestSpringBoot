package hello;

import hello.model.Greeting;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;

@Controller
@EnableAutoConfiguration
public class GreetingController {

  @GetMapping("/greeting")
  public String greetingForm(Model model) {
    model.addAttribute("greeting", new Greeting());
    model.addAttribute("msg",
        "a jar packaging example");
    return "greeting";
  }


  @PostMapping("/greeting")
  public String greetingSubmit(@ModelAttribute Greeting greeting) {

    return "result";
  }


}
